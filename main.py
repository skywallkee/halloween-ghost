# Environment Variables
import os
from dotenv import load_dotenv
import traceback
import configparser

# Discord Bot
import discord
from discord.ext import commands

load_dotenv()

# Bot class
class Bot(commands.Bot):
    def __init__(self):
        super().__init__(description="Halloween Ghost", intents=discord.Intents(guild_messages=True, members=True))
        self.config = configparser.ConfigParser()
        self.config.read('config.ini')
        self.setup_cogs()
        self.normal_chance = 0
        self.ghost_emoji = None

    def setup_cogs(self):
        for cog in self.config['cogs']:
            if self.config['cogs'][cog] == "True":
                self.load_extension(f"cogs.{cog}.main")

    async def on_ready(self):
        self.normal_chance = float(self.config['default']['ghost_chance'])

bot = Bot()
bot.run(os.environ.get("discord_token")) # Requires .env file with discord_token=YOUR TOKEN HERE
