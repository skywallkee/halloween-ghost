import discord
import random
import os
import json
from discord.ext import commands, tasks
from discord.commands import slash_command, SlashCommandGroup
from discord.commands import Option
from datetime import datetime, timedelta
import pytz


__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))

with open(os.path.join(__location__, "ghost_busters.json")) as ghost_busters:
    ghost_busters = json.load(ghost_busters)


class GhostBustersDB():
    def __init__(self):
        pass

    def save_timing(self, timing):
        timings_list = None
        with open(os.path.join(__location__, "timings.json"), "r") as timings:
            timings_list = json.load(timings)
            timings_list.append(timing)
        
        with open(os.path.join(__location__, "timings.json"), "w") as timings:
            json.dump(timings_list, timings)

    
    def get_timings(self):
        timings_list = None
        with open(os.path.join(__location__, "timings.json"), "r") as timings:
            timings_list = json.load(timings)
        return timings_list

    
    def add_point(self, member):
        busters_list = None
        with open(os.path.join(__location__, "ghost_busters.json"), "r") as ghost_busters:
            busters_list = json.load(ghost_busters)
            if str(member.id) in busters_list:
                busters_list[str(member.id)] += 1
            else:
                busters_list[str(member.id)] = 1
        
        with open(os.path.join(__location__, "ghost_busters.json"), "w") as ghost_busters:
            json.dump(busters_list, ghost_busters)

    def get_top5_and_member(self, member):
        busters_list = None
        with open(os.path.join(__location__, "ghost_busters.json"), "r") as ghost_busters:
            busters_list = json.load(ghost_busters)

        sorted_list = [{"id": key, "ghosts": value} for key, value in sorted(busters_list.items(), key=lambda item: item[1])]

        position = 0
        saved_buster = None
        for count, buster in enumerate(sorted_list):
            if buster["id"] == str(member.id):
                position = count
                saved_buster = buster

        if saved_buster == None:
            saved_buster = {"id": member.id, "ghosts": 0}

        return [
            sorted_list[-5:],
            {
                "position": position+1,
                "buster": saved_buster["id"],
                "ghosts": saved_buster["ghosts"]
            }
        ]


class RandomButton(discord.ui.Button):
    def __init__(self, name, emoji, text, row):
        super().__init__(label=name, emoji=emoji, style=discord.ButtonStyle.grey, row=row)
        self.text = text
    
    async def callback(self, interaction):
        await interaction.response.send_message(f"{self.text}", ephemeral=True)


class GhostButton(discord.ui.Button):
    def __init__(self, row):
        super().__init__(label="Boooo!", style=discord.ButtonStyle.grey, emoji=discord.PartialEmoji.from_str("938705178910142485:1026333542092910656"), row=row) # To be chanced emoji with desired one

    async def callback(self, interaction: discord.Interaction):
        response_time = pytz.utc.localize(datetime.now()) - self.view.view_create_time
        timing = int(response_time.seconds*1000) + int(response_time.microseconds/1000)

        if self.view.claimed == True:
            await interaction.response.send_message(content="Someone already caught this ghost :(", ephemeral=True)
            return None
        
        if random.randint(0, 100) <= 25: # 25% chance of ghost getting away
            await interaction.response.send_message(content="Oops, your ghost catcher seems broken, ghost slipped.", ephemeral=True)
            return None

        if timing < 600 and random.randint(0, 100) <= 30: # 30% extra chance for ghost getting away for people being very quick (possibly autoclicker)
            await interaction.response.send_message(content="Hmm, you're quick, but the ghost appears to be way quicker.", ephemeral=True)
            return None

        self.view.claimed = True
        self.view.leaderboards.add_point(interaction.user)
        self.view.leaderboards.save_timing(timing)

        await interaction.response.send_message(f"{interaction.user.mention} has caught me! Response time: {timing}ms")

        self.view.disable_buttons()
        self.emoji = discord.PartialEmoji.from_str("904079955741278318:1026341213013561395") # To be changed with desired emoji after ghost caught
        await self.view.message.edit(view=self.view)
        self.view.stop()


class GhostView(discord.ui.View):
    def __init__(self):
        super().__init__(timeout=10)
        self.leaderboards = GhostBustersDB()
        self.claimed = False
        self.random_buttons = [
            {
                "name": "Fill Button 1!",
                "emoji": discord.PartialEmoji.from_str("name:id"),
                "text": "This is a fill message response!"
            }
        ] # To be added at least 2 fill buttons
        self.view_create_time = pytz.utc.localize(datetime.now())
        self.init_buttons()

    def init_buttons(self):
        orientation = "vertical" if random.randint(0, 1) == 1 else "horizontal"
        button_count = random.randint(1, 3)
        ghost_position = random.randint(0, button_count - 1)

        for count in range(0, button_count):
            row = count if orientation == "vertical" else 0
            if count == ghost_position:
                self.add_item(GhostButton(row=row))
            else:
                random_button = random.choice(self.random_buttons)
                self.add_item(RandomButton(random_button["name"], random_button["emoji"], random_button["text"], row))

    def disable_buttons(self):
        for button in self.children:
            button.disabled = True    

    async def on_timeout(self):
        for button in self.children:
            button.disabled = True
            button.emoji = discord.PartialEmoji.from_str("717130207831654422:1026342120312811660") # To be edited with desired emoji on ghost timeout
        await self.message.edit(view=self)
        await self.message.reply(content="Nobody managed to catch the ghost, so it escaped!")
        self.stop()


class Halloween(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.max_pick = 10000
        self.max_default = 10000
        self.leaderboards = GhostBustersDB()


    halloween = SlashCommandGroup("halloween", "Commands for Halloween")
    
    
    @halloween.command(guild_ids=[], name="display_leaderboards", description="Top 5 Ghost Busters.", default_permission=True)
    async def display_leaderboards(self, ctx):
        busters_list, member_score = self.leaderboards.get_top5_and_member(ctx.user)
        
        if len(busters_list):
            description = ""
            busters_list.reverse()
            for count, buster in enumerate(busters_list):
                description += "```" + str(count + 1) + ". " + ctx.guild.get_member(int(buster["id"])).name + " | " + str(buster["ghosts"]) + " ghosts```"
        else:
            description = "No entries yet"

        description += f"\n\n**Personal placement:** ```{str(member_score['position'])}. {ctx.guild.get_member(int(member_score['buster'])).name} | {str(member_score['ghosts'])} ghosts```"

        embed = discord.Embed(title="Halloween Leaderboards", description="**Top 5 Ghost Busters list:**\n\n" + description)
        await ctx.respond(embed=embed, ephemeral=True)

    
    @halloween.command(guild_ids=[], name="average_timings", description="Average timings.", default_permission=True)
    async def average_timings(self, ctx):
        timings = GhostBustersDB().get_timings()
        total = 0
        for timing in timings:
            total += timing
        await ctx.respond(content=f"Average timing: {total/len(timings)}", ephemeral=True)

    
    @halloween.command(guild_ids=[], name="top_timings", description="Top 10% timings.", default_permission=True)
    async def top_timings(self, ctx):
        timings = GhostBustersDB().get_timings()
        timings.sort()
        timings = timings[:max(1, int(len(timings)*0.1))]
        total = 0
        for timing in timings:
            total += timing
        await ctx.respond(content=f"Top 10% timings: {total/len(timings)}", ephemeral=True)


    async def calculate_chance(self, message):
        last_appearance_divider = 1
        chance = self.bot.normal_chance * 100
        
        utc = pytz.utc

        current_time = datetime.now()
        one_minute = utc.localize(current_time - timedelta(minutes = 1))
        three_minutes = utc.localize(current_time - timedelta(minutes = 3))
        five_minutes = utc.localize(current_time - timedelta(minutes = 5)) # To be changed; Bot hosted on UTC timezone, if in other area, adjust delta

        async for msg in message.channel.history(after=five_minutes):
            chance += 9
            msg_time = msg.created_at
            current_last = 1
            if msg.author.id == self.bot.user.id:
                current_last = 1.3
            if msg_time > three_minutes:
                if msg.author.id == self.bot.user.id:
                    current_last = 1.5
                chance += 8
            if msg_time > one_minute:
                if msg.author.id == self.bot.user.id:
                    current_last = 2
                chance += 7
            if len(msg.content) > 100:
                chance += 3
            if len(msg.content) < 40:
                chance -= 2
            if len(msg.content) < 30:
                chance -= 3
            if len(msg.content) < 20:
                chance -= 5
            if len(msg.content) < 10:
                chance -= 8
            if len(msg.content) < 5:
                chance -= 1
            last_appearance_divider *= current_last

        chance /= last_appearance_divider
        return min(chance, 800)


    @commands.Cog.listener(name="on_message")    
    async def on_message(self, message):
        if message.author.bot == True:
            return None

        chance = await self.calculate_chance(message)
        
        pick = random.randint(0, self.max_pick)
        if pick < chance:
            print(f"{message.author.id} got haunted. {pick}/{chance}")
            view = GhostView()
            reply = await message.reply(view=view)
            self.max_pick = self.max_default
        else:
            print(f"{pick}/{chance}")



def setup(bot):
    bot.add_cog(Halloween(bot))
